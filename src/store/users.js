import Vue from "vue";
import Vuex from "vuex";
import api from "../api/routers.js";

Vue.use(Vuex);

const state = () => ({
    data: [],
    user: [],
    routes: {
        getALL: {
            api: "/users",
        },
        getOne: {
            api: "/users/",
        }, //count
    },
});

const getters = {
    users: state => state.data,
    user: state => state.user
};

const mutations = {
    POPULATE_USERS(state, data) {
        state.data = data
    },
    SHOW_ONE(state, user) {
        state.user = user
    }

};
const actions = {
    FETCH_USERS({ commit }) {
        api.get({ api: state().routes.getALL.api })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    commit("POPULATE_USERS", res.data)
                }
            })
            .catch(err => {
                console.log(err);
            })
    },
    FETCH_USER_ONE({ commit }, id) {
        api.get({ api: state().routes.getOne.api + id })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    commit("SHOW_ONE", res.data)
                }
            })
            .catch(err => {
                console.log(err);
            })

    }
};

export default {
    state,
    getters,
    mutations,
    actions,
};