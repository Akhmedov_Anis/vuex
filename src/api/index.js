import axios from "axios"

export default () => {
    const options = {}

    options.baseURL = "https://jsonplaceholder.typicode.com"
    options.contentType = "application/json"

    const instanse = axios.create(options)

    return instanse

}